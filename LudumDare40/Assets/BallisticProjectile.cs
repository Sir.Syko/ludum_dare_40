﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallisticProjectile : Projectile {

    Vector3 targetPos;
    public Quaternion targetRot;
    float targetDistance;
    float hurtRadius = 5;
	GameObject player;

	// Use this for initialization
	void Start () {
		player = GameObject.FindWithTag("Player");
		targetPos = GameObject.FindWithTag("Player").transform.position;
        targetDistance = Vector3.Distance(transform.position, targetPos);
    }

    protected override void HandleDeath()
    {
        if (Explosion != null)
            Instantiate(Explosion, transform.position, Explosion.transform.rotation);

		if (player != null)
		{
			var playerScript = player.GetComponent<PlayerInfo>();
			if (Vector3.Distance(transform.position, playerScript.transform.position) <= hurtRadius)
			{
				playerScript.hurtPlayer(Damage);
			}
		}

        Destroy(this.gameObject);
    }

    protected override void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag.CompareTo("Obstacle") == 0)
        {
            HandleDeath();
        }
    }

    protected override void FixedUpdate()
    {
        currTime += Time.fixedDeltaTime;

        if (currTime > LifeTime || transform.position.y <= 0.6)
            HandleDeath();
        
        transform.Rotate(transform.right, 1.5f, Space.World);

        transform.position = Vector3.MoveTowards(transform.position, transform.position + transform.forward * 3.0f, targetDistance*Time.fixedDeltaTime);
    }
}
