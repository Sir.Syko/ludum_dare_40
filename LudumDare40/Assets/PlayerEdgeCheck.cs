﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerEdgeCheck : MonoBehaviour {

    public Transform floor_origin;
    public Transform floor_origin2;
    public Transform floor_origin3;
	public Transform edge_check;
	public Transform edge_check1;
	public Transform edge_check2;
	public Transform edge_check3;
	public Transform edge_check4;
	public Transform edge_check5;

	public PlayerMove player_move_script;
    
    private Vector3 floor_direction = new Vector3(0, -1, 0);
    public float ground_distance = 1.1f;
	public float foward_distance = 1.1f;

	// Use this for initialization
	void Start () {
	}

	public bool CheckForwardDirection(Vector3 direction)
	{
		RaycastHit hitInfo;
		LayerMask mask = ~(1 << LayerMask.NameToLayer("Player") |
			1 << LayerMask.NameToLayer("Enemy") | 
			1 << LayerMask.NameToLayer("Projectile") |
			1 << LayerMask.NameToLayer("EnemyProjectile") |
			1 << LayerMask.NameToLayer("Pickables"));

		Physics.Raycast(floor_origin.position, direction, out hitInfo, 20, mask);
		if (hitInfo.transform != null && hitInfo.distance <= foward_distance)
		{
			return false;
		}
		Physics.Raycast(floor_origin2.position, direction, out hitInfo, 20, mask);
		if (hitInfo.transform != null && hitInfo.distance <= foward_distance)
		{
			return false;
		}
		Physics.Raycast(floor_origin3.position, direction, out hitInfo, 20, mask);
		if (hitInfo.transform != null && hitInfo.distance <= foward_distance)
			return false;
		/*Physics.Raycast(edge_check.position, direction, out hitInfo, 20, mask);
		if (hitInfo.transform != null && hitInfo.distance <= foward_distance)
			return false;
		Physics.Raycast(edge_check1.position, direction, out hitInfo, 20, mask);
		if (hitInfo.transform != null && hitInfo.distance <= foward_distance)
			return false;
		Physics.Raycast(edge_check2.position, direction, out hitInfo, 20, mask);
		if (hitInfo.transform != null && hitInfo.distance <= foward_distance)
			return false;
		Physics.Raycast(edge_check3.position, direction, out hitInfo, 20, mask);
		if (hitInfo.transform != null && hitInfo.distance <= foward_distance)
			return false;
		Physics.Raycast(edge_check4.position, direction, out hitInfo, 20, mask);
		if (hitInfo.transform != null && hitInfo.distance <= foward_distance)
			return false;
		Physics.Raycast(edge_check5.position, direction, out hitInfo, 20, mask);
		if (hitInfo.transform != null && hitInfo.distance <= foward_distance)
			return false;*/
		return true;
	}

	public List<RaycastHit> get_bottom_distance()
    {
        List<RaycastHit> hit_info_array = new List<RaycastHit>();

		LayerMask mask = ~(1 << LayerMask.NameToLayer("Player") |
			1 << LayerMask.NameToLayer("Enemy") |
			1 << LayerMask.NameToLayer("Projectile") |
			1 << LayerMask.NameToLayer("EnemyProjectile") |
			1 << LayerMask.NameToLayer("Pickables"));

		RaycastHit hitInfo;
        RaycastHit hitInfo2;
        RaycastHit hitInfo3;

        Physics.Raycast(floor_origin.position, floor_direction, out hitInfo, 20, mask);
        Physics.Raycast(floor_origin2.position, floor_direction, out hitInfo2, 20, mask);
        Physics.Raycast(floor_origin3.position, floor_direction, out hitInfo3, 20, mask);

        hit_info_array.Add(hitInfo);
        hit_info_array.Add(hitInfo2);
        hit_info_array.Add(hitInfo3);

        return hit_info_array;
    }

	// Update is called once per frame
	void FixedUpdate () {
        
        List<RaycastHit> hit_info_list = get_bottom_distance();

        foreach (RaycastHit item in hit_info_list)
        {
            if(item.distance <= ground_distance)
            {
                if (player_move_script.is_grounded()) return;

                Debug.Log("Distance: "+item.distance);
                Debug.Log("object: " + item.collider);
                if (player_move_script.is_falling())
                {
                    player_move_script.setPlayerGrounded(item.point.y);
                    return;
                }
            }
        }

        if(player_move_script.is_grounded())
            player_move_script.setPlayerFalling();
    }
}
