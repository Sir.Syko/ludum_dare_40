﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

	public List<Transform> Prefabs;
	public List<int> PrefabCount;
    private EnemyManager enemyManager;

	// How many times it repeats the spawning
	public int SpawnCount = 1;
	public float SpawnTimer = 0.1f;
	public float SpawnInterval = 0.1f;
	public float EarlyDelay = 0.0f;
	public GameObject SpawnEffect;
	bool canSpawn = true;

	// Use this for initialization
	void Start () {
        enemyManager = GameObject.FindWithTag("GameController").GetComponent<EnemyManager>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private IEnumerator spawn(float countdownValue)
	{
		canSpawn = false;
		yield return new WaitForSeconds(EarlyDelay);
		for (int i = 0; i< Prefabs.Count; i++)
		{
			for (int c = 0; c < PrefabCount[i]; c++)
			{
				Instantiate(SpawnEffect, transform.position, transform.rotation);
				Transform enemy = Instantiate(Prefabs[i], transform.position, transform.rotation) as Transform;
                enemyManager.AddEnemy(enemy.GetComponent<Enemy>());
				yield return new WaitForSeconds(SpawnInterval);
			}
		}
		yield return new WaitForSeconds(countdownValue);
		SpawnCount--;
		canSpawn = true;
	}

	void FixedUpdate()
	{
		if (SpawnCount > 0 && canSpawn)
		{
			StartCoroutine(spawn(SpawnTimer));
		}
	}
}
