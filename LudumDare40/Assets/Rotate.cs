﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour {
	public Vector3 Direction = Vector3.right;
	public float speed = 10.0f;

	// Update is called once per frame
	void FixedUpdate () {
		transform.Rotate(Direction, speed * Time.deltaTime);
	}
}