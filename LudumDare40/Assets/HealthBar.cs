﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBar : MonoBehaviour {

    private PlayerInfo player;
    public Transform health_bar;

    void Awake()
    {
        player = GameObject.FindWithTag("Player").GetComponent<PlayerInfo>();
    }

	void OnGUI()
    {
        health_bar.localScale = new Vector3(player.Health()/100.0f, health_bar.localScale.y, health_bar.localScale.z);
    }
}
