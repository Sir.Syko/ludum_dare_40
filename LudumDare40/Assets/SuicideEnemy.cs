﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if !UNITY_5
using UnityEngine.AI;
#endif

public class SuicideEnemy : Enemy
{

	private void DropLoot()
	{
		Instantiate(LootPrefab, transform.position, transform.rotation);
	}

	private IEnumerator shoot(float countdownValue)
	{
		canShoot = false;
		if (AlwaysShootStraight && pathAgent.target != null)
		{
			Instantiate(Projectile, transform.position, Quaternion.LookRotation((pathAgent.target.position - transform.position).normalized, Vector3.up));
		}
		else
			Instantiate(Projectile, transform.position, transform.rotation);
		DestroySelf();
		yield return new WaitForSeconds(countdownValue);
	}

	public void ReceiveDamage(int value)
	{
		HP -= value;
		if (HP <= 0)
		{
			//Instantiate(explosionPrefab, pos, rot);
			DestroySelf();
			if (splitNum == 0) DropLoot();
		}
	}

	private void DestroySelf()
	{
		PenaltyManager pmanager = GameObject.FindWithTag("GameController").GetComponent<PenaltyManager>();

		int splitChance = pmanager.EnemySplitChance();

		if (splitChance > 0 && splitNum < 2 && Random.Range(0, 100) < splitChance && !mutated)
		{
			//float y_pos = transform.position.y;
			gameObject.transform.localScale *= 0.5f;
			agent.baseOffset *= 2;
			GameObject splitObj = Instantiate(gameObject, transform.position + transform.right, transform.rotation) as GameObject;
			GameObject splitObj2 = Instantiate(gameObject, transform.position - transform.right, transform.rotation) as GameObject;
			splitObj.GetComponent<Enemy>().InitMinorEnemy();
			splitObj2.GetComponent<Enemy>().InitMinorEnemy();
		}

		enemyManager.DestroyEnemy(this);
	}

	public void UpdateSpeed()
	{
		EnemyManager emanager = GameObject.FindWithTag("GameController").GetComponent<EnemyManager>();

		pathAgent.UpdateSpeed(MoveSpeed, emanager.speedModifier);
	}

	public void InitMinorEnemy()
	{
		//transform.position = new Vector3(transform.position.x, y_pos, transform.position.z);
		splitNum++;
	}

	public bool Mutate()
	{
		if (mutated || splitNum > 0) return false;

		mutated = true;
		transform.localScale *= 2f;
		agent.baseOffset *= 0.5f;
		HP *= 5;
		Damage *= 3;
		return true;
	}

	void Start()
	{
		agent = transform.GetComponent<NavMeshAgent>();
		pathAgent = transform.GetComponent<PathAgent>();
		enemyManager = GameObject.FindWithTag("GameController").GetComponent<EnemyManager>();
		pathAgent.UpdateSpeed(MoveSpeed, enemyManager.speedModifier);
	}

	private void FixedUpdate()
	{
		if (pathAgent.target != null)
		{
			var distance = (pathAgent.target.position - transform.position).magnitude;

			if (distance <= WeaponRange && canShoot)
			{
				StartCoroutine(shoot(AttackSpeed));
			}
		}
	}
}
