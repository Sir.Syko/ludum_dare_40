﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{

	public Transform target;
	public float smoothing = 5f;
	Vector3 offset;

	// Use this for initialization
	void Start()
	{
		gameObject.GetComponent<InterpolatedTransformUpdater>().enabled = true;
		gameObject.GetComponent<InterpolatedTransform>().enabled = true;
		target = GameObject.Find("Player").transform;
		offset = transform.position - target.position;
	}

	// Update is called once per frame
	void LateUpdate()
	{
		if (target != null)
		{
			Vector3 targetCamPos = target.position + offset;
			transform.position = Vector3.Lerp(transform.position, targetCamPos, smoothing * Time.fixedDeltaTime);
		}
	}
}
