﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour {

    public float speed_move = 2;
    public float speed_jump = 1f;
    public float gravity = 3f;
    public float max_speed_fall = -20f;
    
    private float vertical_acceleration = 0f;
    private bool grounded = true;
	private PlayerEdgeCheck edgeCheckScript;

    // Use this for initialization
    void Start () {
		gameObject.GetComponent<InterpolatedTransformUpdater>().enabled = true;
		gameObject.GetComponent<InterpolatedTransform>().enabled = true;
		edgeCheckScript = transform.GetComponentInChildren<PlayerEdgeCheck>();
	}

    public void setPlayerGrounded(float offset = 0)
    {
        grounded = true;
        vertical_acceleration = 0;
        transform.position = new Vector3(transform.position.x, offset + 0.6f, transform.position.z);
    }

    public void setPlayerFalling()
    {
        grounded = false;
    }

    public bool is_grounded() { return grounded && vertical_acceleration == 0; }
    public bool is_falling() { return vertical_acceleration < 0; }



	// Update is called once per frame
	void FixedUpdate ()
    {
        float left_right = Input.GetAxis("Horizontal") * speed_move;
        float front_back = Input.GetAxis("Vertical") * speed_move;
        bool jump = Input.GetButton("Jump");

        var xTranslation = Vector3.MoveTowards(transform.position, transform.position + new Vector3(left_right, 0, 0), 0.1f*speed_move);
		if (edgeCheckScript.CheckForwardDirection((xTranslation - transform.position).normalized))
			transform.position = xTranslation;
		var zTranslation = Vector3.MoveTowards(transform.position, transform.position + new Vector3(0, 0, front_back), 0.1f * speed_move);
		if (zTranslation.magnitude > 0 && edgeCheckScript.CheckForwardDirection((zTranslation - transform.position).normalized))
			transform.position = zTranslation;


		transform.position = Vector3.MoveTowards(transform.position, transform.position + new Vector3(0, vertical_acceleration, 0), 0.5f*speed_jump);
		
		if (jump && grounded)
        {
            setPlayerFalling();
            vertical_acceleration = speed_jump;
        }

        if(grounded && vertical_acceleration != 0)
        {
            vertical_acceleration = 0;
        }
        else if(!grounded)
        {
            vertical_acceleration -= gravity * Time.deltaTime;
            vertical_acceleration = Mathf.Max(vertical_acceleration, max_speed_fall);
        }
    }
}
