﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour {
	Aim aimScript;
	public GameObject Projectile;
	public GameObject ShootPoint;
	public float attackSpeed = 0.1f;
	bool canShoot = true;

	private IEnumerator shoot(float countdownValue)
	{
		canShoot = false;
		Instantiate(Projectile, ShootPoint.transform.position, ShootPoint.transform.rotation);
		yield return new WaitForSeconds(countdownValue);
		canShoot = true;
	}

	// Use this for initialization
	void Start () {
		aimScript = transform.GetComponent<Aim>();
	}
	
	private void FixedUpdate()
	{
		if (Input.GetMouseButton(0) && canShoot)
		{
			StartCoroutine(shoot(attackSpeed));
		}
	}
}
