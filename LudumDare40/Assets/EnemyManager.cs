﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour {

    private List<Enemy> enemyList;

    public float speedModifier = 0f;

	// Use this for initialization
	void Start () {
        enemyList = new List<Enemy>();
	}
	
    public void AddEnemy(Enemy enemy)
    {
        enemyList.Add(enemy);
    }

    public void DestroyEnemy(Enemy enemy)
    {
        enemyList.Remove(enemy);
        Destroy(enemy.gameObject);
    }
    
    public void IncreasePenalty()
    {
        speedModifier += 0.5f;
        foreach (Enemy e in enemyList)
        {
            e.UpdateSpeed();
        }
    }

    public void DecreasePenalty()
    {
        speedModifier -= 0.5f;
        foreach (Enemy e in enemyList)
        {
            e.UpdateSpeed();
        }
    }
}
